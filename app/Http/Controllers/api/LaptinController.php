<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use Faker\Provider\Uuid;

use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Models\Pelapor as Pelapor;
use App\Models\Laporan as Laporan;
use App\Models\LaporanRutin as LaporanRutin;
use Validator;

class LaptinController extends ApiController
{
    
    
     public function kaPkm(Request $request) {


    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'bulan_laporan' => 'required',
        ]);

        $errors = $validator->errors();

        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);

            $message = 'Laporan Rutin Kepala Puskesmas Berhasil Diproses';

            $dataLapRutin = array();
            $dataLapRutin[] = ['id'=>'020','val'=>$request->ca_partus];
            $dataLapRutin[] = ['id'=>'021','val'=>$request->bumilresti];
            $dataLapRutin[] = ['id'=>'022','val'=>$request->lahirhdp];
            $dataLapRutin[] = ['id'=>'001','val'=>$request->KI];
            $dataLapRutin[] = ['id'=>'002','val'=>$request->KB];
            $dataLapRutin[] = ['id'=>'003','val'=>$request->GB];
            $dataLapRutin[] = ['id'=>'014','val'=>$request->DBD];
            $dataLapRutin[] = ['id'=>'043','val'=>$request->MLR];
            $dataLapRutin[] = ['id'=>'023','val'=>$request->TB];
            $dataLapRutin[] = ['id'=>'024','val'=>$request->kusta];
            $dataLapRutin[] = ['id'=>'025','val'=>$request->diare];
            $dataLapRutin[] = ['id'=>'026','val'=>$request->pneumonia];
            $dataLapRutin[] = ['id'=>'027','val'=>$request->cpkklinis];
            $dataLapRutin[] = ['id'=>'018','val'=>$request->TN_neo];
            $dataLapRutin[] = ['id'=>'019','val'=>$request->TN_Mtn];
            $dataLapRutin[] = ['id'=>'034','val'=>$request->satu_h];
            $dataLapRutin[] = ['id'=>'035','val'=>$request->satu_k];
            $dataLapRutin[] = ['id'=>'036','val'=>$request->satu_m];
            $dataLapRutin[] = ['id'=>'037','val'=>$request->dua_h];
            $dataLapRutin[] = ['id'=>'038','val'=>$request->dua_k];
            $dataLapRutin[] = ['id'=>'039','val'=>$request->dua_m];
            $dataLapRutin[] = ['id'=>'040','val'=>$request->tiga_h];
            $dataLapRutin[] = ['id'=>'041','val'=>$request->tiga_k];
            $dataLapRutin[] = ['id'=>'042','val'=>$request->tiga_m];
      
            
            foreach($dataLapRutin as $row) {
                $laporan = new Laporan();
                $laporan->laporan_id = Uuid::uuid();
                $laporan->master_laporan_id = $row['id'];
                $laporan->isi_laporan = $row['val'];
                $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
                $laporan->bulan_laporan = $request->bulan_laporan;
                $laporan->nohp =  $request->nohp;
                $laporan->jenis_laporan =  '01';
                $laporan->save();
        
            $laporan_rutin =  new LaporanRutin();
            $laporan_rutin->laporan_rutin_id = Uuid::uuid(); 
            $laporan_rutin->laporan_id      = $laporan->laporan_id;      
            $respond_laporan_rutin = $laporan_rutin->save();
            
            }  
        
            
        }
        else {
             $message = $errors;
             $respond_laporan_rutin = FALSE;
             $laporan = NULL;
        }
        

        echo $this->respondCreated($respond_laporan_rutin,$laporan,$message);

    }
    
    public function kaDinkes(Request $request) {


    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'bulan_laporan' => 'required',
        ]);

        $errors = $validator->errors();

        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);

            $message = 'Laporan Rutin Kepala Puskesmas Berhasil Diproses';

            $dataLapRutin = array();
            $dataLapRutin[] = ['id'=>'020','val'=>$request->ca_partus];
            $dataLapRutin[] = ['id'=>'021','val'=>$request->bumilresti];
            $dataLapRutin[] = ['id'=>'022','val'=>$request->lahirhdp];
            $dataLapRutin[] = ['id'=>'001','val'=>$request->KI];
            $dataLapRutin[] = ['id'=>'002','val'=>$request->KB];
            $dataLapRutin[] = ['id'=>'003','val'=>$request->GB];
            $dataLapRutin[] = ['id'=>'014','val'=>$request->DBD];
            $dataLapRutin[] = ['id'=>'043','val'=>$request->MLR];
            $dataLapRutin[] = ['id'=>'023','val'=>$request->TB];
            $dataLapRutin[] = ['id'=>'024','val'=>$request->kusta];
            $dataLapRutin[] = ['id'=>'025','val'=>$request->diare];
            $dataLapRutin[] = ['id'=>'026','val'=>$request->pneumonia];
            $dataLapRutin[] = ['id'=>'027','val'=>$request->cpkklinis];
            $dataLapRutin[] = ['id'=>'018','val'=>$request->TN_neo];
            $dataLapRutin[] = ['id'=>'019','val'=>$request->TN_Mtn];
            $dataLapRutin[] = ['id'=>'034','val'=>$request->satu_h];
            $dataLapRutin[] = ['id'=>'035','val'=>$request->satu_k];
            $dataLapRutin[] = ['id'=>'036','val'=>$request->satu_m];
            $dataLapRutin[] = ['id'=>'037','val'=>$request->dua_h];
            $dataLapRutin[] = ['id'=>'038','val'=>$request->dua_k];
            $dataLapRutin[] = ['id'=>'039','val'=>$request->dua_m];
            $dataLapRutin[] = ['id'=>'040','val'=>$request->tiga_h];
            $dataLapRutin[] = ['id'=>'041','val'=>$request->tiga_k];
            $dataLapRutin[] = ['id'=>'042','val'=>$request->tiga_m];
            
            
            foreach($dataLapRutin as $row) {
                $laporan = new Laporan();
                $laporan->laporan_id = Uuid::uuid();
                $laporan->master_laporan_id = $row['id'];
                $laporan->isi_laporan = $row['val'];
                $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
                $laporan->bulan_laporan = $request->bulan_laporan;
                $laporan->nohp =  $request->nohp;
                $laporan->jenis_laporan =  '01';
                $laporan->save();
            
        
            $laporan_rutin =  new LaporanRutin();
            $laporan_rutin->laporan_rutin_id = Uuid::uuid(); 
            $laporan_rutin->laporan_id      = $laporan->laporan_id;      
            $respond_laporan_rutin = $laporan_rutin->save();
        
            }
            
        }
        else {
             $message = $errors;
             $respond_laporan_rutin = FALSE;
             $laporan = NULL;
        }
        

        echo $this->respondCreated($respond_laporan_rutin,$laporan,$message);

    }
    
    
    public function dirRS(Request $request) {


    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'bulan_laporan' => 'required',
        ]);

        $errors = $validator->errors();

        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);

            $message = 'Laporan Rutin Kepala Puskesmas Berhasil Diproses';

            $dataLapRutin = array();
            $dataLapRutin[] = ['id'=>'020','val'=>$request->ca_partus];
            $dataLapRutin[] = ['id'=>'029','val'=>$request->BED];
            $dataLapRutin[] = ['id'=>'030','val'=>$request->dr_sp_dasar];
            $dataLapRutin[] = ['id'=>'031','val'=>$request->dr_sp_pnunjang];
            $dataLapRutin[] = ['id'=>'032','val'=>$request->dr_sp_sekolah];
            $dataLapRutin[] = ['id'=>'033','val'=>$request->dr_sp_baru];
            $dataLapRutin[] = ['id'=>'006','val'=>$request->jml_bumil_resti];
            $dataLapRutin[] = ['id'=>'001','val'=>$request->KI];
            $dataLapRutin[] = ['id'=>'002','val'=>$request->KB];
            $dataLapRutin[] = ['id'=>'012','val'=>$request->LM];
            $dataLapRutin[] = ['id'=>'003','val'=>$request->GB];
            $dataLapRutin[] = ['id'=>'014','val'=>$request->DBD];
            $dataLapRutin[] = ['id'=>'043','val'=>$request->MLR];
            $dataLapRutin[] = ['id'=>'018','val'=>$request->TN_neo];
            $dataLapRutin[] = ['id'=>'019','val'=>$request->TN_Mtn];
            $dataLapRutin[] = ['id'=>'015','val'=>$request->CMPK];
            $dataLapRutin[] = ['id'=>'016','val'=>$request->difteri];
            $dataLapRutin[] = ['id'=>'017','val'=>$request->AFP];
            
            foreach($dataLapRutin as $row) {
                $laporan = new Laporan();
                $laporan->laporan_id = Uuid::uuid();
                $laporan->master_laporan_id = $row['id'];
                $laporan->isi_laporan = $row['val'];
                $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
                $laporan->bulan_laporan = $request->bulan_laporan;
                $laporan->nohp =  $request->nohp;
                $laporan->jenis_laporan =  '01';
                $laporan->save();
            }
            
            $laporan_rutin =  new LaporanRutin();
            $laporan_rutin->laporan_rutin_id = Uuid::uuid(); 
            $laporan_rutin->laporan_id      = $laporan->laporan_id;      
            $respond_laporan_rutin = $laporan_rutin->save();
        
            
        }
        else {
             $message = $errors;
             $respond_laporan_rutin = FALSE;
             $laporan = NULL;
        }
        

        echo $this->respondCreated($respond_laporan_rutin,$laporan,$message);

    }
    
    
    public function show()
    {
        $laporan = Laporan::where('jenis_laporan','01')->get();

        $response = [];

        foreach ($laporan as $row) {

        $data = [];
        $data['laporan_id'] = $row->laporan_id;
        $data['bulan_laporan'] = $row->bulan_laporan;
        $data['laporan'] = $row->master_laporan->nama_laporan;
        $data['jumlah'] = $row->isi_laporan;
        $data['nama_pelapor'] = $row->pelapor->nama;
        $data['nohp_pelapor'] = $row->pelapor->nohp;
        $data['email_pelapor'] = $row->pelapor->email;
        $data['identitas'] = $row->pelapor->jenis_pelapor->jenis_pelapor;


        
        $data['created_at'] = $row->created_at;
        $data['updated_at'] = $row->created_at;

        $response[] = $data;
        }

        echo $this->respondData($response);

    }
}
