<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;

use Faker\Provider\Uuid;

use App\Http\Requests;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\api\AccountController;
use App\Models\Pelapor as Pelapor;
use App\Models\Laporan as Laporan;
use App\Models\LaporanKasus as LaporanKasus;
use Validator;


class LapsusController extends ApiController
{
    //

    public function kematianIbu(Request $request) {


    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_ibu' => 'required',
        'umur_ibu' => 'required',
        'desa_tinggal' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();

        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);

            $message = 'Laporan Kematian Ibu Berhasil Diproses';

            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '001';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_ibu        = $request->nama_ibu;      
            $laporan_kasus->jenis_kelamin   = 'P';      
            $laporan_kasus->umur_ibu        = $request->umur_ibu; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }
        

        echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

    public function lahirmati(Request $request) {

    $message = 'Laporan Lahir Mati Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_ibu' => 'required',
        'umur_ibu' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin_bayi' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '012';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_ibu        = $request->nama_ibu;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin_bayi;      
            $laporan_kasus->umur_ibu        = $request->umur_ibu; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }


    public function kematianbayi(Request $request) {

    $message = 'Laporan Kematian Bayi Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_bayi' => 'required',
        'umur_bayi' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin_bayi' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '002';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_bayi        = $request->nama_bayi;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin_bayi;      
            $laporan_kasus->umur_bayi        = $request->umur_bayi; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

    public function giziBuruk(Request $request) {

    $message = 'Laporan Gizi Buruk Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_bayi' => 'required',
        'umur_bayi' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin_bayi' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required',
        'berat_badan_bayi' => 'required',
        'tinggi_badan_bayi'=> 'required'
        
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '003';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_bayi        = $request->nama_bayi;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin_bayi;      
            $laporan_kasus->umur_bayi        = $request->umur_bayi; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $laporan_kasus->berat_badan_bayi = $request->berat_badan_bayi; 
            $laporan_kasus->tinggi_badan_bayi = $request->tinggi_badan_bayi; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

    public function pmtDbd(Request $request) {

    $message = 'Laporan Penyakit Menular Terpilih DBD Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_penderita' => 'required',
        'umur_penderita' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '014';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_penderita        = $request->nama_penderita;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin;      
            $laporan_kasus->umur_penderita        = $request->umur_penderita; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

     public function pmtPneumonia(Request $request) {

    $message = 'Laporan Penyakit Menular Terpilih Pneumonia Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_penderita' => 'required',
        'umur_penderita' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '026';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_penderita        = $request->nama_penderita;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin;      
            $laporan_kasus->umur_penderita        = $request->umur_penderita; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

public function pmtMalaria(Request $request) {

    $message = 'Laporan Penyakit Menular Terpilih Malaria Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_penderita' => 'required',
        'umur_penderita' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '043';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_penderita        = $request->nama_penderita;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin;      
            $laporan_kasus->umur_penderita        = $request->umur_penderita; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }


    public function pmtDiare(Request $request) {

    $message = 'Laporan Penyakit Menular Terpilih Diare Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_penderita' => 'required',
        'umur_penderita' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '025';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_penderita        = $request->nama_penderita;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin;      
            $laporan_kasus->umur_penderita        = $request->umur_penderita; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

    public function pmt(Request $request) {

    $message = 'Laporan Penyakit Menular Terpilih Berhasil Diproses';    

    $validator = Validator::make($request->all(), [
        'nohp' => 'required',
        'nama_penderita' => 'required',
        'umur_penderita' => 'required',
        'desa_tinggal' => 'required',
        'jenis_kelamin' => 'required',
        'tgl_ditemukan' => 'required',
        'lokasi_ditemukan' => 'required'
        ]);

        $errors = $validator->errors();
        
        if(count($errors)<1) {
            
            $this->_authPhone($request->nohp);
            
            $laporan = new Laporan();
            $laporan->laporan_id = Uuid::uuid();
            $laporan->master_laporan_id = '004';
            $laporan->isi_laporan = 1;
            $laporan->pelapor_id =  Pelapor::getPelaporIdByNoHP($request->nohp);
            $laporan->bulan_laporan = $request->bulan_laporan;
            $laporan->nohp =  $request->nohp;
            $laporan->jenis_laporan =  '02';
            $laporan->save();
        
            $laporan_kasus =  new LaporanKasus();
            $laporan_kasus->laporan_kasus_id = Uuid::uuid(); 
            $laporan_kasus->laporan_id      = $laporan->laporan_id;      
            $laporan_kasus->nama_penderita        = $request->nama_penderita;      
            $laporan_kasus->jenis_kelamin   = $request->jenis_kelamin;      
            $laporan_kasus->umur_penderita        = $request->umur_penderita; 
            $laporan_kasus->desa_tinggal    = $request->desa_tinggal; 
            $laporan_kasus->tgl_ditemukan   = $request->tgl_ditemukan; 
            $laporan_kasus->lokasi_ditemukan = $request->lokasi_ditemukan; 
            $respond_laporan_kasus = $laporan_kasus->save();
        }
        else {
             $message = $errors;
             $respond_laporan_kasus = FALSE;
             $laporan_kasus = NULL;
        }

         echo $this->respondCreated($respond_laporan_kasus,$laporan_kasus,$message);

    }

    public function show()
    {
        $laporan = Laporan::where('jenis_laporan','02')->get();

        $response = [];

        foreach ($laporan as $row) {
        $data = [];
        $data['laporan_id'] = $row->laporan_id;
        $data['laporan'] = $row->master_laporan->nama_laporan;
        $data['nama_pelapor'] = $row->pelapor->nama;
        $data['nohp_pelapor'] = $row->pelapor->nohp;
        $data['email_pelapor'] = $row->pelapor->email;
        $data['identitas'] = $row->pelapor->jenis_pelapor->jenis_pelapor;


        foreach($row->laporan_kasus as $data_laporan_kasus) {

        $laporan_kasus = [];
        $laporan_kasus['laporan_kasus_id'] = $data_laporan_kasus->laporan_kasus_id;
        $laporan_kasus['nama_penderita'] = $data_laporan_kasus->nama_penderita;
        $laporan_kasus['umur_penderita'] = $data_laporan_kasus->umur_penderita;
        $laporan_kasus['nama_bayi'] = $data_laporan_kasus->nama_bayi;
        $laporan_kasus['jenis_kelamin'] = $data_laporan_kasus->jenis_kelamin;
        $laporan_kasus['berat_badan_bayi'] = $data_laporan_kasus->berat_badan_bayi;
        $laporan_kasus['tinggi_badan_bayi'] = $data_laporan_kasus->tinggi_badan_bayi;
        $laporan_kasus['umur_bayi'] = $data_laporan_kasus->umur_bayi;
        $laporan_kasus['nama_ibu'] = $data_laporan_kasus->nama_ibu;
        $laporan_kasus['umur_ibu'] = $data_laporan_kasus->umur_ibu;
        $laporan_kasus['desa_tinggal'] = $data_laporan_kasus->desa_tinggal;
        $laporan_kasus['tgl_ditemukan'] = $data_laporan_kasus->tgl_ditemukan;
        $laporan_kasus['lokasi_ditemukan'] = $data_laporan_kasus->lokasi_ditemukan;
        }        

        $data['laporan_kasus'] = $laporan_kasus;
        $data['created_at'] = $row->created_at;
        $data['updated_at'] = $row->created_at;

        $response[] = $data;
        }

        echo $this->respondData($response);

    }

    public function rollback(Request $request)
    {
        $laporan = Laporan::where('laporan_id', $request->laporan_id);
        $data = $laporan->delete();
        
        $laporan_kasus = LaporanKasus::where('laporan_kasus_id', $request->laporan_id);

        echo $this->respondDeleted($data);

    }
    


    
}
