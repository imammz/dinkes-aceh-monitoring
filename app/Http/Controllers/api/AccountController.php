<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Models\JenisPelapor as JenisPelapor;
use App\Models\Pelapor as Pelapor;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;

class AccountController extends ApiController
{
    //

    public function register(Request $request)
    {

    $data = false;
        $pelapor = null;
        $identitas = $request->identitas;

            $jenis_pelapor = JenisPelapor::where('identitas', $identitas)->first();

if(count($jenis_pelapor)>0) {
            $pelapor = new Pelapor;
            $pelapor->pelapor_id = Uuid::uuid();
            $pelapor->nama = $request->nama;
            $pelapor->nohp = $request->nohp;
            $pelapor->jenis_pelapor_id = $jenis_pelapor->jenis_pelapor_id;

            if (isset($request->nik)) {
                $pelapor->nik = $request->nik;
            }
            if (isset($request->email)) {
                $pelapor->email = $request->email;
            }
            if (!isset($request->email)) {
                $pelapor->email = $pelapor->nohp . '@dinkesprovaceh';
            }
            if (isset($request->tempat_lahir)) {
                $pelapor->tempat_lahir = $request->tempat_lahir;
            }
            if (isset($request->tanggal_lahir)) {
                $pelapor->tanggal_lahir = $request->tanggal_lahir;
            }
            if (isset($request->kode_desa)) {
                $pelapor->kode_desa = $request->kode_desa;
            }
            if (isset($request->kode_kecamatan)) {
                $pelapor->kode_kecamatan = $request->kode_kecamatan;
            }
            if (isset($request->kode_kab)) {
                $pelapor->kode_kab = $request->kode_kab;
            }
            if (isset($request->kode_prov)) {
                $pelapor->kode_prov = $request->kode_prov;
            }

            if (isset($request->nama_desa)) {
                $pelapor->nama_desa = $request->nama_desa;
            }
            if (isset($request->nama_kecamatan)) {
                $pelapor->nama_kecamatan = $request->nama_kecamatan;
            }
            if (isset($request->nama_kab)) {
                $pelapor->nama_kab = $request->nama_kab;
            }
            if (isset($request->nama_prov)) {
                $pelapor->nama_prov = $request->nama_prov;
            }
            if (isset($request->kode_puskesmas)) {
                $pelapor->kode_puskesmas = $request->kode_puskesmas;
            }
            if (isset($request->kode_sirs)) {
                $pelapor->kode_sirs = $request->kode_sirs;
            }

            if ($this->checkPhoneNumber($pelapor->nohp)) {
            $data = $pelapor->save();
                $message = 'Register Success';
            } else {
                $message = 'Usualy Phone Number Already Register';
            }

}
else {

    $message = 'Identitas Salah';
}

        echo $this->respondCreated($data, $pelapor, $message);

    }

    public function rollback(Request $request)
    {

        $pelapor = Pelapor::where('nohp', $request->nohp);
    $data = $pelapor->delete();

    if($data) {
        $getPelapor = $pelapor->withTrashed()->first();
        $getPelapor->nohp = rand(0, 99) . '#' . $getPelapor->nohp; //agar saat create new dengan No HP yang tidak error Unique
        $getPelapor->email = rand(0, 99) . '#' . $getPelapor->email; //agar saat create new dengan No HP yang tidak error Unique
        $getPelapor->save();
             }
             
        echo $this->respondDeleted($data);

    }

    public function cancelRollback(Request $request)
    {
        //pending
    }

    public function checkPhoneNumber($number)
    {
        $pelapor = Pelapor::where('nohp', $number);

        if ($pelapor->count() == 0) {
            return true;
        } else {
            return false;
        }

    }

    public function serviceCheckPhoneNumber($number) {

        if($this->checkPhoneNumber($number)) {
            echo $this->respondData(['nohp'=>$number],'Nomor HP Belum Terdaftar');
        }
        else {
            echo $this->respondData(null,'Nomor HP Sudah Terdaftar');
        }

    }
    


    public function allMember()
    {
        $pelapor = Pelapor::All();

        $response = [];

        foreach ($pelapor as $row) {
        $data = [];
        $data['nama'] = $row['nama'];
        $data['nohp'] = $row['nohp'];
        $data['nik'] = $row['nik'];
        $data['email'] = $row['email'];
        $data['tempat_lahir'] = $row['tempat_lahir'];
        $data['tanggal_lahir'] = $row['tanggal_lahir'];
        $data['identitas_jenis_pelapor'] = $row->jenis_pelapor->identitas;
        $data['jenis_pelapor'] = $row->jenis_pelapor->jenis_pelapor;
        $data['kode_puskesmas'] = $row['kode_puskesmas'];
        $data['kode_sirs'] = $row['kode_sirs'];
        $data['kode_desa'] = $row['kode_desa'];
        $data['kode_kecamatan'] = $row['kode_kecamatan'];
        $data['kode_kab'] = $row['kode_kab'];
        $data['kode_prov'] = $row['kode_prov'];
        $data['nama_desa'] = $row['nama_desa'];
        $data['nama_kecamatan'] = $row['nama_kecamatan'];
        $data['nama_kab'] = $row['nama_kab'];
        $data['nama_prov'] = $row['nama_prov'];
        $response[] = $data;
        }

        echo $this->respondData($response);

    }

}
