<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LaporanKasus
 */
class LaporanRutin extends Model
{
    use SoftDeletes;
    
    protected $table = 'laporan_rutin';

    public $timestamps = true;

    public $primaryKey = 'laporan_rutin_id';
    public $incrementing = false;

    protected $fillable = [
        'laporan_id'
    ];

    protected $guarded = [];



    public function laporan() {
       return $this->belongsTo('App\Models\Laporan','laporan_id');
    }
    
        
}