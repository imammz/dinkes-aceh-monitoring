<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class MasterLaporan
 */
class MasterLaporan extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_laporan';

    protected $primaryKey = 'master_laporan_id';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'kode_laporan',
        'nama_laporan',
        'jenis_laporan_id'
    ];

    protected $guarded = [];

    public function jenis_laporan() {
      return  $this->belongsTo('App\Models\JenisLaporan','jenis_laporan_id');
    }

    public function laporan() {
       return $this->hasMany('App\Models\Laporan','laporan_id');
    }

    
        
}