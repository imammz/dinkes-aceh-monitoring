<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MasterKabupaten
 */
class MasterKabupaten extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_kabupaten';

    protected $primaryKey = 'kabupaten_kode';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'provinsi_kode',
        'tipe',
        'nama_kabupaten'
    ];

    protected $guarded = [];

        
}