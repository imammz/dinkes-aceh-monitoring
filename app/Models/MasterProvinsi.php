<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MasterProvinsi
 */
class MasterProvinsi extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_provinsi';

    protected $primaryKey = 'provinsi_kode';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'nama_provinsi'
    ];

    protected $guarded = [];

        
}