<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class JenisPelapor
 */
class JenisPelapor extends Model
{
    use SoftDeletes;
    
    protected $table = 'jenis_pelapor';

    protected $primaryKey = 'jenis_pelapor_id';
    
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'jenis_pelapor'
    ];

    protected $guarded = [];

    public function pelapor() {
        $this->hasmany('App\Model\Pelapor','jenis_pelapor_id');
    }

        
}