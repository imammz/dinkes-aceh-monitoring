<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pelapor
 */
class Pelapor extends Model
{

    use SoftDeletes;


    protected $table = 'pelapor';

    protected $primaryKey = 'pelapor_id';
    public $incrementing = false;
        
    
	public $timestamps = true;
    

    protected $fillable = [
        'nama',
        'no_ktp',
        'email',
        'tempat_lahir',
        'tanggal_lahir',
        'kode_desa',
        'kode_kecamatan',
        'kode_kab',
        'kode_prov',
        'nama_desa',
        'nama_kecamatan',
        'nama_kab',
        'nama_prov',
        'jenis_pelapor_id'
    ];

    protected $guarded = [];

    public function laporan() {
        
        return $this->hasmany('App\Models\Laporan','pelapor_id');  

    } 
    
    public function jenis_pelapor()
    {
        return $this->belongsTo('App\Models\JenisPelapor', 'jenis_pelapor_id');
    }
    
    public static function getPelaporIdByNoHP($nohp) {

        $pelapor = Pelapor::where('nohp',$nohp)->first();

        return $pelapor->pelapor_id;
    
    }

    

}