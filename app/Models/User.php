<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class User
 */
class User extends Model
{
    use SoftDeletes;
    
    protected $table = 'users';

    protected $primaryKey = 'users_id';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'email',
        'password',
        'remember_token'
    ];

    protected $guarded = [];

        
}