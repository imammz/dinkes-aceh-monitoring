<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class JenisLaporan
 */
class JenisLaporan extends Model
{
    use SoftDeletes;
    
    protected $table = 'jenis_laporan';

    protected $primaryKey = 'jenis_laporan_id';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'jenis_laporan'
    ];

    protected $guarded = [];

    
    public function master_laporan()
    {
        return $this->hasMany('App\Models\MasterLaporan', 'master_laporan_id', 'local_key');
    }
    

        
}