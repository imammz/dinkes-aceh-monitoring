<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class LaporanKasus
 */
class LaporanKasus extends Model
{
    use SoftDeletes;
    
    protected $table = 'laporan_kasus';

    public $timestamps = true;

    public $primaryKey = 'laporan_kasus_id';
    public $incrementing = false;

    protected $fillable = [
        'laporan_id',
        'nama_penderita',
        'nama_bayi',
        'jenis_kelamin',
        'berat_badan_bayi',
        'umur_bayi',
        'nama_ibu',
        'umur_ibu',
        'desa_tinggal',
        'tgl_ditemukan'
    ];

    protected $guarded = [];



    public function laporan() {
       return $this->belongsTo('App\Models\Laporan','laporan_id');
    }
    
        
}