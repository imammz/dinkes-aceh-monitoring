<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Laporan
 */
class Laporan extends Model
{
    use SoftDeletes;
    
    protected $table = 'laporan';

    protected $primaryKey = 'laporan_id';
    public $incrementing = false;
	
    public $timestamps = true;

    protected $fillable = [
        'master_laporan_id',
        'isi_laporan',
        'pelapor_id',
        'jenis_laporan',
        'no_ktp'
    ];

    protected $guarded = [];


    public function master_laporan() {
        return $this->belongsTo('App\Models\MasterLaporan','master_laporan_id');
    }

    public function pelapor() {
       return $this->belongsTo('App\Models\Pelapor','pelapor_id');
    }

    public function laporan_kasus(){
        return $this->hasMany('App\Models\LaporanKasus','laporan_id');
    }







        
}