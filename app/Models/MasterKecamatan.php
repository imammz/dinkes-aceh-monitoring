<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MasterKecamatan
 */
class MasterKecamatan extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_kecamatan';

    protected $primaryKey = 'kecamatan_kode';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'kabupaten_kode',
        'nama_kecamatan'
    ];

    protected $guarded = [];

        
}