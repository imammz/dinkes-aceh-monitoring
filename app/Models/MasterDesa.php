<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MasterDesa
 */
class MasterDesa extends Model
{
    use SoftDeletes;
    
    protected $table = 'master_desa';

    protected $primaryKey = 'desa_kode';
    public $incrementing = false;

	public $timestamps = true;

    protected $fillable = [
        'kecamatan_kode',
        'nama_desa'
    ];

    protected $guarded = [];

        
}