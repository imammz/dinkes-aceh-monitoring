-- MySQL dump 10.16  Distrib 10.1.16-MariaDB, for osx10.6 (i386)
--
-- Host: localhost    Database: db_dinkesaceh_monitoring
-- ------------------------------------------------------
-- Server version	10.1.16-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `db_dinkesaceh_monitoring`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `db_dinkesaceh_monitoring` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_dinkesaceh_monitoring`;

--
-- Table structure for table `jenis_laporan`
--

DROP TABLE IF EXISTS `jenis_laporan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_laporan` (
  `jenis_laporan_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_laporan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jenis_laporan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_laporan`
--

LOCK TABLES `jenis_laporan` WRITE;
/*!40000 ALTER TABLE `jenis_laporan` DISABLE KEYS */;
INSERT INTO `jenis_laporan` VALUES ('01','Laporan Rutin','2016-10-09 20:30:57',NULL,NULL),('02','Laporan Kasus','2016-10-09 20:30:57',NULL,NULL);
/*!40000 ALTER TABLE `jenis_laporan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_pelapor`
--

DROP TABLE IF EXISTS `jenis_pelapor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_pelapor` (
  `jenis_pelapor_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_pelapor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identitas` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`jenis_pelapor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_pelapor`
--

LOCK TABLES `jenis_pelapor` WRITE;
/*!40000 ALTER TABLE `jenis_pelapor` DISABLE KEYS */;
INSERT INTO `jenis_pelapor` VALUES ('01','Bidan Desa','bides','2016-10-09 20:30:57',NULL,NULL),('01A','Bidan Kordinator','bikor','2016-10-09 20:30:57',NULL,NULL),('02','Kepala Puskesmas','ka.pusk','2016-10-09 20:30:57',NULL,NULL),('03','Direktur Rumah Sakit','dir.rs','2016-10-09 20:30:57',NULL,NULL),('04','Kepala Dinas Kesehatan Kab','kadinkes','2016-10-09 20:30:57',NULL,NULL);
/*!40000 ALTER TABLE `jenis_pelapor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laporan`
--

DROP TABLE IF EXISTS `laporan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laporan` (
  `laporan_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `master_laporan_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `isi_laporan` decimal(8,2) NOT NULL,
  `pelapor_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `nohp` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ktp` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
  `bulan_laporan` varchar(8) COLLATE utf8_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`laporan_id`),
  KEY `laporan_master_laporan_id_index` (`master_laporan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laporan`
--

LOCK TABLES `laporan` WRITE;
/*!40000 ALTER TABLE `laporan` DISABLE KEYS */;
INSERT INTO `laporan` VALUES ('08e0dd77-2f2d-339a-a279-685ac9620a78','001',7.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('105d4ad1-5f9a-3c2f-aa9d-4c9555161356','018',2.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('176d6016-8160-3a61-a883-d8b4285a91da','014',3.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('17af1757-4fa8-3c0d-b269-ed49d6b51bdc','027',0.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('18c1e26b-529e-3076-acf0-c4e4b16544b4','003',1.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('25dc9b5e-8753-3c1a-955a-3388038d64c3','035',6.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('2c47da9e-3510-378a-a4cf-a68216028bd0','002',3.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('4bd84a3a-8dec-35c0-afb5-47d041ee6548','039',3.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('4d641d4a-06a9-356c-896e-3d5ba0368e4a','043',1.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('565b069a-07b0-3f6c-b8ee-f7385d54241c','025',5.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('69b891e5-bde3-3c35-81f0-9f198c79e189','020',2.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('6e76a8a7-e2f7-3e0f-b4b3-8e256a366ee8','026',1.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('7446f11c-8058-3883-986c-75ac652a3643','019',0.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('7a7a9bcb-1100-3e1e-bdc0-538f04832ff1','034',4.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('97c8b03c-643f-3b32-aad5-a899e36ef7b4','023',3.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('a7f98c26-d629-34c5-8bc0-36b89e0e817c','040',5.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('adcae034-b868-31b0-bfd1-d606cc36315f','036',7.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('b57884f7-5dac-35cf-b0f9-4952e96b017f','037',8.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('ba2d2227-cb1e-3e62-b354-a9051527c355','024',4.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('c86cef16-6bf6-3b90-b1ef-6822ebba311f','042',1.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('ceaca006-42d9-3ed4-ac23-3b66cf4d8cc0','021',4.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('d435ab35-539a-3b11-b225-44b4184f5e41','041',2.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('e712ce22-2586-3c60-8b67-db9b0533e6f6','038',2.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('fdd993c9-728b-3b41-a424-40c0f2486572','022',7.00,'995b630c-51f2-3caf-afdd-378783921b2f',NULL,'','9','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL);
/*!40000 ALTER TABLE `laporan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laporan_kasus`
--

DROP TABLE IF EXISTS `laporan_kasus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laporan_kasus` (
  `laporan_kasus_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `laporan_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `nama_penderita` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'hanya untuk laporan DBD, Malaria, Pneumonia, Diare',
  `umur_penderita` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_bayi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin` enum('L','P') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Untuk bayi atau penderita (tergantung laporan)',
  `berat_badan_bayi` decimal(4,2) DEFAULT NULL,
  `tinggi_badan_bayi` decimal(5,2) DEFAULT NULL,
  `umur_bayi` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_ibu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `umur_ibu` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desa_tinggal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tgl_ditemukan` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `lokasi_ditemukan` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`laporan_kasus_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laporan_kasus`
--

LOCK TABLES `laporan_kasus` WRITE;
/*!40000 ALTER TABLE `laporan_kasus` DISABLE KEYS */;
/*!40000 ALTER TABLE `laporan_kasus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laporan_rutin`
--

DROP TABLE IF EXISTS `laporan_rutin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laporan_rutin` (
  `laporan_rutin_id` char(36) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `laporan_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`laporan_rutin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laporan_rutin`
--

LOCK TABLES `laporan_rutin` WRITE;
/*!40000 ALTER TABLE `laporan_rutin` DISABLE KEYS */;
INSERT INTO `laporan_rutin` VALUES ('087e070e-438d-30ce-a8b8-6e4273341aac','105d4ad1-5f9a-3c2f-aa9d-4c9555161356','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('18b5cba4-81d6-3eda-ba9f-2dacec87db6c','fdd993c9-728b-3b41-a424-40c0f2486572','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('28561715-4e29-3d73-a734-2ca22b96e831','17af1757-4fa8-3c0d-b269-ed49d6b51bdc','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('3147e483-822d-33fa-af87-ef0cc9f31922','08e0dd77-2f2d-339a-a279-685ac9620a78','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('381acbb8-eae4-3122-9ab8-24f19c72397b','d435ab35-539a-3b11-b225-44b4184f5e41','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('3a6578b1-cc64-345f-bcec-6152a3806ef3','ba2d2227-cb1e-3e62-b354-a9051527c355','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('3b5bb744-f74d-3207-a1fc-53cbcf10d859','565b069a-07b0-3f6c-b8ee-f7385d54241c','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('41ae5973-c488-3cc7-a557-913a11315da3','176d6016-8160-3a61-a883-d8b4285a91da','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('48f50c3e-0d2f-3ea6-9341-893ccfdfab5f','6e76a8a7-e2f7-3e0f-b4b3-8e256a366ee8','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('64083456-3010-3428-bbfb-8f036550dc52','c86cef16-6bf6-3b90-b1ef-6822ebba311f','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('6b1ef3a7-0e85-3f25-9117-82032a164967','4d641d4a-06a9-356c-896e-3d5ba0368e4a','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('6f045892-f6ff-39ce-b385-3c33fbba803a','ceaca006-42d9-3ed4-ac23-3b66cf4d8cc0','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('706be4e3-91c1-32da-89c9-2a82112d136d','18c1e26b-529e-3076-acf0-c4e4b16544b4','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('73de3879-f284-3606-94d9-c344271e4d44','25dc9b5e-8753-3c1a-955a-3388038d64c3','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('85f9533d-dc05-3469-ae9e-888831889165','7446f11c-8058-3883-986c-75ac652a3643','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('92e5f6b8-d5cb-3df2-ae8c-ce3a920b945a','69b891e5-bde3-3c35-81f0-9f198c79e189','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('aba7ef2d-dde9-3d81-8ff3-2920beb22430','b57884f7-5dac-35cf-b0f9-4952e96b017f','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('c1dbe1db-6a9c-3c53-81e3-df46ab5b7ea1','2c47da9e-3510-378a-a4cf-a68216028bd0','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('d67f79fc-c9df-37cb-a4d2-c07d11ec4f94','4bd84a3a-8dec-35c0-afb5-47d041ee6548','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('e384d2fe-859d-3e51-a421-72aec725a56b','a7f98c26-d629-34c5-8bc0-36b89e0e817c','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('e724bcd5-de3c-3004-93c5-c2b0b7d34e5c','97c8b03c-643f-3b32-aad5-a899e36ef7b4','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('ea1a6cf7-b06c-363f-ba56-e62a065b770f','adcae034-b868-31b0-bfd1-d606cc36315f','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('fc0bf099-d3e7-3f9d-93d0-03a59f529790','7a7a9bcb-1100-3e1e-bdc0-538f04832ff1','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL),('fcfa126e-42d3-3aa2-8438-cbb95a0465ed','e712ce22-2586-3c60-8b67-db9b0533e6f6','2016-11-10 21:49:54','2016-11-10 21:49:54',NULL);
/*!40000 ALTER TABLE `laporan_rutin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_desa`
--

DROP TABLE IF EXISTS `master_desa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_desa` (
  `desa_kode` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `kecamatan_kode` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `nama_desa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`desa_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_desa`
--

LOCK TABLES `master_desa` WRITE;
/*!40000 ALTER TABLE `master_desa` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_desa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kabupaten`
--

DROP TABLE IF EXISTS `master_kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kabupaten` (
  `kabupaten_kode` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `provinsi_kode` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `tipe` enum('1','2') COLLATE utf8_unicode_ci NOT NULL COMMENT '1=>Kabupaten, 2=>Kota',
  `nama_kabupaten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`kabupaten_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kabupaten`
--

LOCK TABLES `master_kabupaten` WRITE;
/*!40000 ALTER TABLE `master_kabupaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_kecamatan`
--

DROP TABLE IF EXISTS `master_kecamatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_kecamatan` (
  `kecamatan_kode` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `kabupaten_kode` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `nama_kecamatan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`kecamatan_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_kecamatan`
--

LOCK TABLES `master_kecamatan` WRITE;
/*!40000 ALTER TABLE `master_kecamatan` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_kecamatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_laporan`
--

DROP TABLE IF EXISTS `master_laporan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_laporan` (
  `master_laporan_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `kode_laporan` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nama_laporan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_laporan_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`master_laporan_id`),
  UNIQUE KEY `master_laporan_kode_laporan_unique` (`kode_laporan`),
  KEY `master_laporan_jenis_laporan_id_index` (`jenis_laporan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_laporan`
--

LOCK TABLES `master_laporan` WRITE;
/*!40000 ALTER TABLE `master_laporan` DISABLE KEYS */;
INSERT INTO `master_laporan` VALUES ('001','KI','Kematian Ibu','02','2016-10-09 20:30:57',NULL,NULL),('002','KB','kematian bayi','01','2016-10-09 20:30:57',NULL,NULL),('003','GB','Gizi Buruk','02','2016-10-09 20:30:57',NULL,NULL),('004','PMT','Penyakit Menular Terpilih','02','2016-10-09 20:30:57',NULL,NULL),('005','KK','Jumlah KK','01','2016-10-09 20:30:57',NULL,NULL),('006','jml.bumil.resti','jumlah ibu hamil resiko tinggi yg terdata melalui pengobatan pasien di rs','01','2016-10-09 20:30:57',NULL,NULL),('007','waktupartus','catatan perkiraan waktu melahirkan pasien ibu hamil resiko tinggi','01','2016-10-09 20:30:57',NULL,NULL),('008','jmlpartus','jumlah persalinan resiko tinggi bulan laporan','01','2016-10-09 20:30:57',NULL,NULL),('009','normalpartus','jumlah bumil resti yg melahirkan melalui cara normal','01','2016-10-09 20:30:57',NULL,NULL),('010','SC','jmlh dgn cara sesar','01','2016-10-09 20:30:57',NULL,NULL),('011','KL','kematian ibu','01','2016-10-09 20:30:57',NULL,NULL),('012','LM','bayi lahir dalam kondisi meninggal(lahir mati)','01','2016-10-09 20:30:57',NULL,NULL),('013','BL','Jumlah Bayi Lahir','01','2016-10-09 20:30:57',NULL,NULL),('014','DBD','Demam Berdarah','01','2016-10-09 20:30:57',NULL,NULL),('015','CMPK','campak','01','2016-10-09 20:30:57',NULL,NULL),('016','difteri','difteri','01','2016-10-09 20:30:57',NULL,NULL),('017','AFP','AFP','01','2016-10-09 20:30:57',NULL,NULL),('018','TN.neo','Tetanus Neo','01','2016-10-09 20:30:57',NULL,NULL),('019','TN.Mtn','Tetanus MTN','01','2016-10-09 20:30:57',NULL,NULL),('020','ca.partus','jumlah perkiraan calon ibu melahirkan','01','2016-10-09 20:30:57',NULL,NULL),('021','bumilresti','ibu hamil dengan resiko tinggi','01','2016-10-09 20:30:57',NULL,NULL),('022','lahirhdp','bayi lahir selamat(hidup)','01','2016-10-09 20:30:57',NULL,NULL),('023','TB','tuber colosis','01','2016-10-09 20:30:57',NULL,NULL),('024','kusta','kusta','01','2016-10-09 20:30:57',NULL,NULL),('025','diare','diare','01','2016-10-09 20:30:57',NULL,NULL),('026','pneumonia','pneumonia','01','2016-10-09 20:30:57',NULL,NULL),('027','cpkklinis','campak klinis','01','2016-10-09 20:30:57',NULL,NULL),('028','JB','Jumlah Bides','01','2016-10-09 20:30:57',NULL,NULL),('029','BED','Tempat Tidur Pasien yang tersedia di RS','01','2016-10-09 20:30:57',NULL,NULL),('030','dr.sp.dasar','Jumlah Tenaga Dokter Spesialis Dasar Yang ada di RS','01','2016-10-09 20:30:57',NULL,NULL),('031','dr.sp.pnunjang','Jumlah Tenaga Dokter Spesialis Yang ada di RS selain dasar','01','2016-10-09 20:30:57',NULL,NULL),('032','dr.sp.sekolah','Dokter RS yang sedang ada dalam masa sekolah (lepas dinas)','01','2016-10-09 20:30:57',NULL,NULL),('033','dr.sp.baru','Dokter Spesialis Selain Dasar dan penunjang sifatnya baru masuk menjadi tenaga medis RS','01','2016-10-09 20:30:57',NULL,NULL),('034','1.H','status dokter jaga di puskesmas malam hari (H)','01','2016-10-09 20:30:57',NULL,NULL),('035','1.K','status dokter jaga di puskesmas malam hari (K)','01','2016-10-09 20:30:57',NULL,NULL),('036','1.M','status dokter jaga di puskesmas malam hari (M)','01','2016-10-09 20:30:57',NULL,NULL),('037','2.H','status puskesmas kebersihan dll (H)','01','2016-10-09 20:30:57',NULL,NULL),('038','2.K','status puskesmas kebersihan dll (K)','01','2016-10-09 20:30:57',NULL,NULL),('039','2.M','status puskesmas kebersihan dll (M)','01','2016-10-09 20:30:57',NULL,NULL),('040','3.H','status puskesmas pembantu (pustu) (H)','01','2016-10-09 20:30:57',NULL,NULL),('041','3.K','status puskesmas pembantu (pustu) (K)','01','2016-10-09 20:30:57',NULL,NULL),('042','3.M','status puskesmas pembantu (pustu) (M)','01','2016-10-09 20:30:57',NULL,NULL),('043','MLR','Malaria','02','2016-10-09 20:30:57',NULL,NULL);
/*!40000 ALTER TABLE `master_laporan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `master_provinsi`
--

DROP TABLE IF EXISTS `master_provinsi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `master_provinsi` (
  `provinsi_kode` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `nama_provinsi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`provinsi_kode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `master_provinsi`
--

LOCK TABLES `master_provinsi` WRITE;
/*!40000 ALTER TABLE `master_provinsi` DISABLE KEYS */;
/*!40000 ALTER TABLE `master_provinsi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_09_22_223254_create_pelapor',1),('2016_09_22_223412_create_jenis_pelapor',1),('2016_09_22_223859_create_jenis_laporan',1),('2016_09_22_223957_create_master_laporan',1),('2016_09_22_225658_create_master_desa',1),('2016_09_22_225713_create_master_kecamatan',1),('2016_09_22_225735_create_master_kabupaten',1),('2016_09_22_225750_create_master_provinsi',1),('2016_09_22_234626_create_laporan',1),('2016_09_26_081027_create_laporan_kasus',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pelapor`
--

DROP TABLE IF EXISTS `pelapor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pelapor` (
  `pelapor_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nohp` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `nik` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `tempat_lahir` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `kode_desa` int(11) DEFAULT '0',
  `kode_kecamatan` int(11) DEFAULT '0',
  `kode_kab` int(11) DEFAULT '0',
  `kode_prov` int(11) DEFAULT '0',
  `nama_desa` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_kecamatan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_kab` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_prov` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_pelapor_id` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `kode_puskesmas` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_sirs` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_kabupaten` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`pelapor_id`),
  UNIQUE KEY `pelapor_nohp_unique` (`nohp`),
  UNIQUE KEY `pelapor_email_unique` (`email`),
  KEY `pelapor_jenis_pelapor_id_index` (`jenis_pelapor_id`),
  KEY `pelapor_kode_puskesmas_index` (`kode_puskesmas`),
  KEY `pelapor_kode_sirs_index` (`kode_sirs`),
  KEY `pelapor_kode_kabupaten_index` (`kode_kabupaten`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pelapor`
--

LOCK TABLES `pelapor` WRITE;
/*!40000 ALTER TABLE `pelapor` DISABLE KEYS */;
INSERT INTO `pelapor` VALUES ('3fa22a14-c463-31e8-8674-4a42f0655ba4','dona','90#081299818199',NULL,'54#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-12 23:45:18','2016-10-13 02:13:59','2016-10-13 02:13:59'),('433e7bcd-841f-3592-b450-facdb30b7794','dona','73#0812998181',NULL,'88#0812998181@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-09 20:33:06','2016-10-09 20:33:24','2016-10-09 20:33:24'),('469deb7a-ae2a-31af-a9f1-8a7527027667','dona','84#081299818199',NULL,'88#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-09 20:42:18','2016-10-09 20:42:29','2016-10-09 20:42:29'),('4a96ded9-caa0-3ace-b8bf-9e77d0dbdbf9','dona','42#081299818199',NULL,'62#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:07:07','2016-10-13 03:15:51','2016-10-13 03:15:51'),('5a7b31f9-1e0d-3edc-b85a-d2eef998d7fc','whita','081299999',NULL,'081299999@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01A',NULL,NULL,NULL,'2016-10-09 20:32:34','2016-10-09 20:32:34',NULL),('6d4e847f-3a32-33b3-95ed-c43bda034c68','dona','67#081299818199',NULL,'37#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 02:15:07','2016-10-13 03:07:07','2016-10-13 03:07:07'),('797085ef-afbe-3a4c-8f6f-3ecd8e1a9955','dona','65#081299818199',NULL,'73#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:22:55','2016-10-13 03:23:02','2016-10-13 03:23:02'),('7bf252ee-ce7f-3c98-adfc-d54eedb3b735','Raisa','08129818181',NULL,'08129818181@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-09 20:45:27','2016-10-09 20:45:27',NULL),('995b630c-51f2-3caf-afdd-378783921b2f','imam','081389607795',NULL,'081389607795@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'03',NULL,NULL,NULL,'2016-10-09 20:32:01','2016-10-09 20:32:01',NULL),('abbc1d27-ae63-35e7-9421-95866de5e87c','dona','61#081299818199',NULL,'90#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 02:13:59','2016-10-13 02:15:07','2016-10-13 02:15:07'),('b42297f8-8de7-3e9d-921b-efabeb1a8015','dona','46#081299818199',NULL,'9#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:23:02','2016-11-10 19:50:48','2016-11-10 19:50:48'),('b58834a7-38e4-3f18-b019-2389855b242d','dona','08129981777',NULL,'08129981777@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:22:30','2016-10-13 03:22:30',NULL),('b7a5332e-c0c1-3020-a210-5f2134a43762','dona','55#081299818199',NULL,'46#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:22:49','2016-10-13 03:22:55','2016-10-13 03:22:55'),('bbb15f71-730a-34a7-90da-c90552f4be8c','dona','53#081299818199',NULL,'39#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:15:51','2016-10-13 03:22:33','2016-10-13 03:21:30'),('ca476116-7527-3e1e-9e9b-6ac91e270bfc','dona','73#081299818199',NULL,'98#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-12 23:34:29','2016-10-12 23:45:18','2016-10-12 23:45:18'),('da52792c-3c53-38b8-9285-66ca9de78a8a','dona','91#081299818199',NULL,'52#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-13 03:22:44','2016-10-13 03:22:49','2016-10-13 03:22:49'),('eebb2158-ecd0-3bc3-9743-28a7b6bef206','dona','0812998181',NULL,'0812998181@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-09 20:42:04','2016-10-09 20:42:04',NULL),('f96b1360-1ed2-3780-94a4-2670582de289','dona','7#081299818199',NULL,'15#081299818199@dinkesprovaceh',NULL,NULL,0,0,0,0,NULL,NULL,NULL,NULL,'01',NULL,NULL,NULL,'2016-10-09 20:43:41','2016-10-12 23:34:28','2016-10-12 23:34:28');
/*!40000 ALTER TABLE `pelapor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `users_id` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`users_id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('de4b900c-ca7f-316c-8842-afc1699eda14','imammz@ymail.com','$2y$10$SJcxZeE07m/j2iPolZuTBuTHO/sqWHLWNwK9Z.1oIZ1G7rc2fcmEq',NULL,'2016-10-09 20:30:57',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-11 11:50:01
