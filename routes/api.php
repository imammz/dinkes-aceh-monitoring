<?php

use Illuminate\Http\Request;
use Faker\Provider\Uuid;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'account'], function () {

        Route::post('/register', 'api\AccountController@register'); //DONE
        /*
        nama | Not NULL
        nohp | Not NULL
        identitas | Not Null
         */

        Route::delete('/rollback', 'api\AccountController@rollback'); //DONE
        /*
        nohp
         */
        Route::post('/cancel_rollback', 'api\AccountController@rollbackCancel'); //PENDING
        /*
        nohp
         */

        Route::get('/check_phone_number/{number}', 'api\AccountController@serviceCheckPhoneNumber')->name('checkPhoneNumber'); //DONE
        
        Route::get('/allmember', 'api\AccountController@allMember')->name('allMember'); //DONE

    });

    Route::group(['prefix' => 'laporan_kasus'], function () {


        Route::post('/kematianibu', 'api\LapsusController@kematianIbu');
        /*
        nohp
        nama_ibu
        umur_ibu
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::post('/lahirmati', 'api\LapsusController@lahirMati');
        /*
        nohp
        nama_ibu
        umur_ibu
        jenis_kelamin_bayi | L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::post('/kematianbayi', 'api\LapsusController@kematianBayi');
        /*
        nohp
        nama_bayi
        umur_bayi
        jenis_kelamin_bayi | L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::post('/giziburuk', 'api\LapsusController@giziBuruk');
        /*
        nohp
        nama_bayi
        umur_bayi
        jenis_kelamin_bayi | value=>L/P
        berat_badan_bayi  | in KG
        tinggi_badan_bayi  | in CM
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::post('/pmt_dbd', 'api\LapsusController@pmtDbd');
        //Penyakit Menular Terpilih
        /*
        nohp
        nama_penderita
        umur_penderita
        jenis_kelamin | value=>L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::post('/pmt_malaria', 'api\LapsusController@pmtMalaria');
        //Penyakit Menular Terpilih
        /*
        nohp
        nama_penderita
        umur_penderita
        jenis_kelamin | value=>L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */
        Route::post('/pmt_pneumonia', 'api\LapsusController@pmtPneumonia');
        //Penyakit Menular Terpilih
        /*
        nohp
        nama_penderita
        umur_penderita
        jenis_kelamin | value=>L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */
        Route::post('/pmt_diare', 'api\LapsusController@pmtDiare');
        //Penyakit Menular Terpilih
        /*
        nohp
        nama_penderita
        umur_penderita
        jenis_kelamin | value=>L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */
         Route::post('/pmt', 'api\LapsusController@pmt');
        //Penyakit Menular Terpilih
        /*
        nohp
        nama_penderita
        umur_penderita
        jenis_kelamin | value=>L/P
        desa_tinggal
        tgl_ditemukan
        lokasi_ditemukan
         */

        Route::get('/data', 'api\LapsusController@show');
        Route::delete('/rollback', 'api\LapsusController@rollback');
        /*
        laporan_id
        */

    });

    Route::group(['prefix' => 'laporan_rutin'], function () {

        Route::post('/ka_pkm', 'api\laptinController@kaPkm');
        /*
        nohp
        bulan_laporan
        ca.partus
        bumilresti
        lahirhdp
        KI
        KB
        GB
        DBD
        malaria
        TB
        kusta
        diare
        pneumonia
        cpkklinis
        TN
        tetanus
        1.h
        1.k
        1.m
        2.h
        2.k
        2.m
        3.h
        3.k
        3.m
         */
        Route::post('/ka_dinkes', 'api\laptinController@kaDinkes');
        /*
        nohp
        bulan_laporan
        ca.partus
        bumilresti
        lahirhdp
        KI
        KB
        GB
        DBD
        malaria
        TB
        kusta
        diare
        pneumonia
        cpkklinis
        TN
        tetanus
        1.h
        1.k
        1.m
        2.h
        2.k
        2.m
        3.h
        3.k
        3.m
         */
        Route::post('/dirrs', 'api\laptinController@dirRS');
        /*
        nohp
        bed
        dr.sp.dasar
        dr.sp.pnunjang
        dr.sp.sekolah
        dr.sp.baru
        waktu.bumil.resti // merujuk pada bulan_laporan untuk jml.bumil.resti
        jml.bumil.resti
        KI
        KB
        LM
        GB
        DBD
        Malaria
        cmpk
        difteri
        AFP
        TN.neo
        TN.Mtn
         */

        Route::get('/data', 'api\laptinController@show');
        Route::delete('/rollback', 'api\laptinController@rollback');
        /*
    laporan_id
     */

    });

});
