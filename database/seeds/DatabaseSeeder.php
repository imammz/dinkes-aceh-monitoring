<?php

use Illuminate\Database\Seeder;
use Faker\Provider\Uuid;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
      
        $this->users();
        $this->jenisPelapor();
        $this->jenisLaporan();
        $this->masterLaporan();
        


    }


    private function users() { 		
         DB::table('users')->delete();
        $json = File::get("database/data/users.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('users')->insert([
            'users_id' => Uuid::uuid(),
            'password' => bcrypt('admin'),
            'email'=> $obj->email,
            'created_at'=>date('Y-m-d H:i:s')

          ]);
        }
    }

    private function jenisLaporan() { 		
         DB::table('jenis_laporan')->delete();
        $json = File::get("database/data/jenis_laporan.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('jenis_laporan')->insert([
            'jenis_laporan_id'=> $obj->jenis_laporan_id,
            'jenis_laporan'=> $obj->jenis_laporan,
            'created_at'=>date('Y-m-d H:i:s')
          ]);
        }
    }

    private function jenisPelapor() { 		
         DB::table('jenis_pelapor')->delete();
        $json = File::get("database/data/jenis_pelapor.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('jenis_pelapor')->insert([
            'jenis_pelapor_id'=> $obj->jenis_pelapor_id,
            'jenis_pelapor'=> $obj->jenis_pelapor,
            'identitas'=> $obj->identitas,
            'created_at'=>date('Y-m-d H:i:s')
          ]);
        }
    }

    private function masterLaporan() { 		
         DB::table('master_laporan')->delete();
        $json = File::get("database/data/master_laporan.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('master_laporan')->insert([
            'master_laporan_id'=> $obj->master_laporan_id,
            'kode_laporan'=> $obj->kode_laporan,
            'nama_laporan'=> $obj->nama_laporan,
            'jenis_laporan_id'=> $obj->jenis_laporan_id,
            'created_at'=>date('Y-m-d H:i:s')
           ]);
        }
    }

    private function masterDesa() { 		
          DB::table('master_desa')->delete();
        $json = File::get("database/data/master_desa.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('master_desa')->insert([
            'desa_kode'=> $obj->desa_kode,
            'kecamatan_kode'=> $obj->kecamatan_kode,
          ]);
        }
    }

    private function masterKecamatan() { 		
          DB::table('master_kecamatan')->delete();
        $json = File::get("database/data/master_kecamatan.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('master_kecamatan')->insert([
            'kecamatan_kode'=> $obj->kecamatan_kode,
            'kabupateb_kode'=> $obj->kabupaten_kode,
            'tipe'=> $obj->tipe,
            'nama_kecamatan'=> $obj->nama_kecamatan,
          ]);
        }
    }

    private function masterKabupaten() { 		
          DB::table('master_kabupaten')->delete();
        $json = File::get("database/data/master_kabupaten.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('master_kabupaten')->insert([
            'kabupaten_kode'=> $obj->kabupaten_kode,
            'provinsi_kode'=> $obj->provinsi_kode,
            'tipe'=> $obj->tipe,
            'nama_kabupaten'=> $obj->nama_kabupaten,
          ]);
        }
    }

    private function masterProvinsi() { 		
         DB::table('master_provinsi')->delete();
        $json = File::get("database/data/master_provinsi.json");
        $data = json_decode($json);
        foreach ($data as $obj) {
          DB::table('master_provinis')->insert([
            'provinsi_kode'=> $obj->provinsi_kode,
            'nama_provinsi'=> $obj->nama_provinsi,
          ]);
        }
    }


}
