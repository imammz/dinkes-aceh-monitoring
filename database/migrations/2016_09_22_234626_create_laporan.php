<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan', function (Blueprint $table) {
            //
            $table->uuid('laporan_id')->primary();
            $table->string('master_laporan_id',32)->index();
            $table->decimal('isi_laporan',8,2);
            $table->uuid('pelapor_id');
            $table->string('no_ktp');
            $table->string('bulan_laporan',8);
            $table->timestamps();
            $table->softDeletes();   


        });

   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('laporan');
    }
}
