<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterLaporan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_laporan', function (Blueprint $table) {
            //
            $table->string('master_laporan_id', 32)->primary();
            $table->string('kode_laporan', 64)->unique();
            $table->string('nama_laporan');
            $table->string('jenis_laporan_id', 32)->index();
            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_laporan');
    }
}
