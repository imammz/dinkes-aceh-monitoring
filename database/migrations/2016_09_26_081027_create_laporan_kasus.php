<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanKasus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporan_kasus', function (Blueprint $table) {
            $table->uuid('laporan_kasus_id')->primary();
            $table->uuid('laporan_id');
            $table->string('nama_penderita')->comment('hanya untuk laporan DBD, Malaria, Pneumonia, Diare')->nullable();
            $table->string('umur_penderita')->nullable();
            $table->string('nama_bayi')->nullable();
            $table->enum('jenis_kelamin',['L','P'])->comment('Untuk bayi atau penderita (tergantung laporan)');
            $table->decimal('berat_badan_bayi',2,2)->nullable();
            $table->decimal('tinggi_badan_bayi',3,2)->nullable();
            $table->string('umur_bayi',4)->nullable();
            $table->string('nama_ibu')->nullable();
            $table->string('umur_ibu',4)->nullable();
            $table->string('desa_tinggal');
            $table->string('tgl_ditemukan',16);
            $table->string('lokasi_ditemukan',64);
            $table->timestamps();


        });

   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laporan_kasus');
    }
}
