<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelapor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelapor', function (Blueprint $table) {
            //
            $table->uuid('pelapor_id')->primary();
            $table->string('nama');
            $table->string('nohp',16)->unique();
            $table->string('nik',64)->nullable();
            $table->string('email',128)->unique();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->integer('kode_desa')->default(0);
            $table->integer('kode_kecamatan')->default(0);
            $table->integer('kode_kab')->default(0);
            $table->integer('kode_prov')->default(0);
            $table->string('nama_desa')->nullable();
            $table->string('nama_kecamatan')->nullable();
            $table->string('nama_kab')->nullable();
            $table->string('nama_prov')->nullable();
            $table->string('jenis_pelapor_id',32)->index();
            $table->string('kode_puskesmas',32)->nullable()->index();
            $table->string('kode_sirs',16)->nullable()->index();
            $table->string('kode_kabupaten',4)->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
           
        });




    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::drop('pelapor');
    }
}
