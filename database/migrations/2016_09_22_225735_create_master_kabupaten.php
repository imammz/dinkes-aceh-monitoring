<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterKabupaten extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_kabupaten', function (Blueprint $table) {
            //
            $table->string('kabupaten_kode',4)->primary();
            $table->string('provinsi_kode',2);
            $table->enum('tipe',['1','2'])->comment('1=>Kabupaten, 2=>Kota');
            $table->string('nama_kabupaten');
            $table->timestamps();
            $table->softDeletes();              
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_kabupaten');
    }
}
