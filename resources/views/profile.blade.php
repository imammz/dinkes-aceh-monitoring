<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="author" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>Muh Imam Mudzakkir</title>
        <!-- Favicon -->
        <link rel="shortcut icon" href="{{asset('assets/profile/images/favicon.png')}}">
        <!-- ##### STYLESHEETS ##### -->
        <!-- Bootstrap -->
        <link href="{{asset('assets/profile/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Linea Icons -->
        <link href="{{asset('assets/profile/linea/styles.css')}}" rel="stylesheet">
        <!-- Owl Carousel -->
        <link href="{{asset('assets/profile/css/owl.carousel.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/profile/css/owl.theme.default.min.css')}}" rel="stylesheet">
        <!-- Magnific popup -->
        <link href="{{asset('assets/profile/css/magnific-popup.min.css')}}" rel="stylesheet">
        <!-- Preloader -->
        <link href="{{asset('assets/profile/css/preloader.min.css')}}" rel="stylesheet">
        <!-- Main styles -->
        <link href="{{asset('assets/profile/css/styles.min.css')}}" rel="stylesheet">
        <!-- Responsive -->
        <link href="{{asset('assets/profile/css/responsive.min.css')}}" rel="stylesheet">
        <!-- Font Awesome Icons -->
        <link href="{{asset('assets/profile/maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}" rel="stylesheet">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,700,900" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- Preloader -->
        <div class="preloader">
          <div class="loader"></div>
        </div>
        <!-- end Preloader -->

        <!-- Toggle button  -->
        <button class="toggle-btn">
            <span class="lines"></span>
        </button>
        <!-- end Toggle button -->

        <!-- Sticky Sidebar menu -->
        <div class="sticky-sidebar-menu">
            <div class="sidebar-top">
                <img src="{{asset('assets/profile/images/imam-sidebar.jpg')}}" alt="">
                <h4>Muhammad Imam Mudzakkir</h4>
                <h5>Full Stack Developer</h5>
            </div>
            <!-- Sidebar menu links -->
            <div class="sidebar-menu">
                <ul class="nav">
                    <li class="active">
                        <a href="#home">Home</a>
                    </li>
                    <li>
                        <a href="#about">About</a>
                    </li>
                    <li>
                        <a href="#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a href="#services">Services</a>
                    </li>
                   
                    <li>
                        <a href="#contact">Contact</a>
                    </li>
                </ul>
            </div><!-- end Sidebar menu links -->
            <!-- Sidebar Social media links -->
            <div class="sidebar-social">
                <ul>
                    <li>
                        <a href="">
                            <i class="fa fa-facebook" target="_blank"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/in/imammz/" target="_blank">
                            <i class="fa fa-linkedin"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- end Sidebar Social media links -->
        </div>
        <!-- end Sticky Sidebar menu -->

        <!-- Home section -->
        <section id="home" class="fill">
            <div class="home-background">
                <div class="dark-layer">
                    <div class="home-middle-content">
                        <h1>I'm a Full Stack Developer</h1>
                        <h4>With over 8 years of experience</h4>
                    </div>
                    <!-- Scroll down button -->
                    <div class="scroll-down">
                        <a href="#about"><i class="fa fa-angle-double-down"></i></a>
                    </div><!-- end Scroll down button -->
                </div>
            </div>
        </section>
        <!-- end Home section -->

        <!-- About section -->
        <section id="about">
            <div class="about-background">
                <h2 class="section-title">About Me</h2>
                <div class="row">
                    <div class="col-xs-12 col-lg-6 about-img">
                        <img src="{{asset('assets/profile/images/avatar.jpg')}}" alt=""><!-- Avatar Image -->
                    </div>
                    <div class="col-xs-12 col-lg-6 about-content">
                        <h4>Full Stack Developer</h4>
                        <h2>Muhammad Imam Mudzakkir</h2>
                        <p>I have a passion and strong desire to solve difficult problems in all fields of industry, business, and government that can be solved with IT solutions</p>
                    </div>
                </div><!-- end row -->
                <!-- Skills -->
                <div class="row skills-row">
                    <!-- Skill box 1 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>PHP (laravel, codeigniter, yii), J2EE #backend</h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"><span>100%</span>
                            </div>
                        </div>
                    </div>
                    <!-- Skill box 2 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>Angular, VuJS, HTML5, CSS3 #frontend </h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span>80%</span>
                            </div>
                        </div>
                    </div>
                    <!-- Skill box 3 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>Relational Database SQL Server, MariaDB, Postgres SQL</h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"><span>90%</span>
                            </div>
                        </div>
                    </div>
 <!-- Skill box 4 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>No SQL Database</h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"><span>80%</span>
                            </div>
                        </div>
                    </div>
                    <!-- Skill box 5 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>Enterprise Web Development </h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"><span>90%</span>
                            </div>
                        </div>
                    </div>
                    <!-- Skill box 6 -->
                    <div class="col-xs-12 col-sm-4 skill-box">
                        <h4>Hybrid Mobile Development</h4>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="9000" aria-valuemin="0" aria-valuemax="100" style="width: 75%;"><span>75%</span>
                            </div>
                        </div>
                    </div>
                </div><!-- end row -->
                <!-- end Skills -->
            </div>
        </section>
        <!-- end About section -->

        <!-- Facts -->
        <section id="facts">
            <div class="facts-background">
                <div class="row">
                    <!-- Fact box 1 -->
                    <div class="col-xs-12 col-sm-3 facts-box">
                        <h2><span class="counter">289</span></h2>
                        <h4>Cups of Coffee</h4>
                    </div>
                    <!-- Fact box 2 -->
                    <div class="col-xs-12 col-sm-3 facts-box">
                        <h2><span class="counter">25</span></h2>
                        <h4>Software Projects Done</h4>
                    </div>
                  
                    <!-- Fact box 4 -->
                    <div class="col-xs-12 col-sm-3 facts-box">
                        <h2><span class="counter">110</span></h2>
                        <h4>Slices of Pizza</h4>
                    </div><!-- end Fact box 4 -->
                </div><!-- end row -->
            </div>
        </section>
        <!-- end Facts -->

        <!-- Portfolio -->
        <section id="portfolio">
            <div class="portfolio-background">
                <h2 class="section-title">Portfolio</h2>
              
                <div id="mix-container">
                    <iframe src="{{asset('portfolio 2017.pdf')}}" width="100%" height="500" frameBorder="0">Browser not compatible.</iframe>
                </div>
            </div>
        </section>
        <!-- end Portfolio -->

        <!-- Services -->
        <section id="services">
            <div class="services-background">
                <h2 class="section-title">What I Do</h2>
                <div class="row">
                    <!-- Service box 1 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-gear"></i>
                            <h4>Web Development</h4>
                            <p>Web Application Development with PHP (Laravel, Yii, Codeigniter) or JAVA to Enterprise Edition (J2EE)</p>
                        </div>
                    </div>
                    <!-- Service box 2 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-webpage-img-txt"></i>
                            <h4>Database Design</h4>
                            <p>Entity Relational Database in SQL Server, Maria DB and PostgreSQL.</p>
                        </div>
                    </div>
             </div>
 <div class="row">
                    <!-- Service box 3 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-photo"></i>
                            <h4>Full Rest API</h4>
                            <p>Backend Aplication Development with Full Rest API as Web Services Integration Platform.</p>
                        </div>
                    </div>
                    <!-- Service box 4 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-world"></i>
                            <h4>E-Goverment</h4>
                            <p>E-Goverment Specialist such as : Sistem Informasi Kepegawaian, Sistem Kinerja Pegawai, Sistem Persuratan & Kearsipan, Sistem Pelayanan Perizinan, Sistem Kesehatan, Sistem Pelaporan and ETC.</p>
                        </div>
                    </div><!-- end Service box 4 -->
</div>
 <div class="row">
                     <!-- Service box 4 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-world"></i>
                            <h4>ERP</h4>
                            <p>Enterprise Resource Planning such as : Human Resource Information System, Accounting System, Finance System, Document Management System ETC.</p>
                        </div>
                    </div><!-- end Service box 4 -->
              <!-- Service box 4 -->
                    <div class="col-xs-12 col-sm-6 service-box">
                        <div>
                            <i class="icon icon-basic-world"></i>
                            <h4>CRM</h4>
                            <p>Enterprise Customer Relationship Management.</p>
                        </div>
                    </div><!-- end Service box 4 -->
</div>

            </div>
        </section>
        <!-- end Services -->

       

        <!-- Testimonial -->
        <section id="testimonial">
            <div class="testimonial-background">
                <div class="testimonial-dark-layer">
                    <div class="testimonial-container">
                        <div id="testimonialSlider" class="owl-carousel">
                            <!-- Testimonial box 1 -->
                            <div class="testimonial-box">
                                <span>
                                    <i class="fa fa-quote-right"></i>
                                </span>
                                <h4>John Ruskin</h4>
                                <p>When Love and Skill Work Together, Expect a Masterpiece.</p>
                            </div>
                            <!-- Testimonial box 2 -->
                            <div class="testimonial-box">
                                <span>
                                    <i class="fa fa-quote-right"></i>
                                </span>
                                <h4>Steve Jobs</h4>
                                <p>My Job is to not be easy on people. My job is to make theme better.</p>
                            </div>
                            <!-- Testimonial box 3 -->
                            <div class="testimonial-box">
                                <span>
                                    <i class="fa fa-quote-right"></i>
                                </span>
                                <h4>John Johnson</h4>
                                <p>First, solve the problem. Then, write the code.</p>
                            </div><!-- end Testimonial box 3 -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end Testimonial -->

       

        <!-- Contact -->
        <section id="contact">
            <div class="contact-background">
                <h2 class="section-title">Contact Me</h2>
                <div class="row">
                    <!-- Contact form -->
                    <div class="col-xs-12 col-sm-6 contact-form-box">
<!--                        <form method="post" id="contactform">
                            <div class="input-style">
                                <div>
                                    <input type="text" id="name" name="name" placeholder="Name">
                                </div>
                                <div>
                                    <input type="email" id="email" name="email" placeholder="E-Mail">
                                </div>
                            </div>
                            <div class="text-style">
                                <textarea name="message" id="message" placeholder="Message"></textarea>
                            </div>
                            <input type="submit" id="submit" class="submit-style" name="submit" value="Send Message">
                            <div class="submit-result"> It will only appear after pushing submit button 
                                <p id="success">Your Message has been sent!</p>
                                <p id="error">Something went wrong, go back and try again!</p>
                            </div>
                        </form>-->
                    </div><!-- end Contact form -->
                    <!-- Contact Info -->
                    <div class="col-xs-12 col-sm-12 contact-info">
                        <ul>
                            <li>
                                <h4>Whatsapp</h4>
                                <p>+(62) 813-8960-7795</p>
                            </li>
                            <li>
                                <h4>Email</h4>
                                <p>imammz@ymail.com</p>
                            </li>
                            <li>
                                <h4>Address</h4>
                                <p>Jakarta, Indonesia</p>
                            </li>
                        </ul>
                    </div><!-- end Contact Info -->
                </div><!-- end row -->
            </div>
        </section>
        <!-- end Contact -->

        <!-- Footer -->
        <footer>
            <div class="footer-background">
                <p>&copy; 2017 Muhammad Imam Mudzakkir</p>
            </div>
        </footer>
        <!-- end Footer -->
        
        
        <!-- By Muhammad Imam Mudzakkir -->
        
       <!-- jQuery Library -->
        <script src="{{asset('assets/profile/code.jquery.com/jquery-1.12.4.min.js')}}"></script>
        <!-- Bootstrap js -->
        <script src="{{asset('assets/profile/maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js')}}"></script>
        <!-- Retina Graphics -->
        <script src="{{asset('assets/profile/js/retina.min.js')}}"></script>
        <!-- Magnific Popup -->
        <script src="{{asset('assets/profile/js/jquery.magnific-popup.min.js')}}"></script>
        <!-- Theme Plugins -->
        <script src="{{asset('assets/profile/js/theme-plugins.min.js')}}"></script>
        <!-- Custom Scripts -->
        <script src="{{asset('assets/profile/js/scripts.min.js')}}"></script>
    </body>
</html>